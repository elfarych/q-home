import axios from 'axios'
import server from 'src/config/server'

export async function init ({ dispatch }) {
  console.log('init')
  await dispatch('loadSlides')
}

export async function loadSlides ({ commit }) {
  try {
    await axios.get(`${server.serverURL}/slides/`)
      .then(response => commit('mutationSlides', response.data))
  } catch (e) {
    console.log(e)
  }
}
