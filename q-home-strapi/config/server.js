module.exports = ({ env }) => ({
  host: env('HOST', '192.168.0.155'),
  port: env.int('PORT', 1337),
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', '443c3a12c58e7c94a7b86851b21b93d7'),
    },
  },
});
