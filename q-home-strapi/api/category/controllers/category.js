'use strict';
const { sanitizeEntity } = require('strapi-utils');

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async find(ctx) {
    let categories
    if (ctx.query._q) {
      categories = await strapi.services.category.search(ctx.query)
    } else {
      categories = await strapi.services.category.find(ctx.query)
    }
    const sanitizeCategories = categories.map(entity => sanitizeEntity(entity, {model: strapi.models.category}))
    return sanitizeCategories.map(item => {
      return {
        id: item.id,
        name: item.name,
        image: item.image.formats.thumbnail.url,
        slug: item.slug,
        productsCount: item.products.length
      }
    })
  }
}
