'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

module.exports = {
  async find(ctx) {
    let slides
    if (ctx.query._q) {
      slides = await strapi.services.slide.search(ctx.query)
    } else {
      slides = await strapi.services.slide.find(ctx.query)
    }
    return slides.map(item => {
      return {
        id: item.id,
        title: item.title,
        image: item.image.formats.large.url,
        slug: item.slug
      }
    })
  }
};
